  <?php
  class findTranslation  {
    public $string;
    public $compared;
    public $newValue = array();

    // print_r($this->string);
    // die();
    public function __construct($arr1, $arr2) {
      $this->string = $arr1;
      $this->compared = $arr2;
    }
   
    function doCheck($array = [])
    {
      $loopdata = !empty($array) ? $array : $this->string;
      foreach($loopdata as $key=>$value) {
        if(is_array($value)) {
          $this->doCheck($value);
        } else {
          $this->search($this->compared, $key, $value);
        }
      }
    }
    function search($array, $keydata, $valuedata) 
    {
      foreach ($array as $key=>$value) {
        if (is_array($value)) {
          $this->search($value, $keydata, $valuedata);
        } else {
          if ($key == $keydata && $valuedata == $value) {
            $this->push($keydata, $valuedata);
            return true;
            break;
          }
        }
      }
    }
    function push($key, $value)
    {
      array_push($this->newValue, array($key=>$value));
    }
}
$traslationAvailable = ['de', 'es', 'fr', 'id', 'it', 'ja', 'ko', 'pt', 'ru', 'vi', 'zh', 'th'];

foreach($traslationAvailable as $key => $value) {
 
  $string = file_get_contents("./placeCurrentTranslation/en.json");
  $string = json_decode($string, TRUE);
  
  $compared = file_get_contents("./placeCurrentTranslation/$value.json");
  $compared = json_decode($compared, TRUE);
  
  $object  = new findTranslation($string, $compared);
  $object->doCheck();
  $new = json_encode($object->newValue, JSON_UNESCAPED_UNICODE);
  $myfile = fopen("./notTranslated/$value.json", "w") or die("Unable to open file!");
  fwrite($myfile, $new);
  fclose($myfile);
}

  
  // $new = json_encode($object->newValue, JSON_UNESCAPED_UNICODE);
  // echo($new);