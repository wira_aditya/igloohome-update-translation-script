  <?php
  // header('Content-Type: application/json');
  class findTho  {
    public $mainString;
    public $compared;
    public $newValue = array();
    public $parentkey = '';
    public $childkey = '';

    
    // print_r($this->string);
    // die();
    public function __construct($arr1, $arr2) {
      $this->mainString = $arr1;
      $this->compared = $arr2;
    }
    
    function mapingData($arr =[]) {
      $loopData = !empty($arr) ? $arr : $this->mainString;
      foreach($loopData as $key=>$value) {
        if (is_array($value)) {
          
          $this->mapingData($value);
        } else {
          $this->compare($value);
          // print_r('1 looping');
          // break;
        }
      }
    }

    function compare($data) {

      // print_r("compared called $data");
      // print_r('</br>');
      $updatedString = preg_replace('/\{{(.*?)\}}/', '', $data);
      $isSame = false;
      foreach ($this->compared as $key => $value) {
        $updateRowValue = preg_replace('/\{{(.*?)\}}/', '', $value);
        if ($updatedString == $updateRowValue) {
          // print_r('somethin  ');
          $isSame = true;
          break;
          // return;
        }
      }
      if(!$isSame) {
        $this->pushToObject($data);
      }
    }

    // pussyng into the object
    function pushToObject($value) {
      array_push($this->newValue, $value);
      }
    
}
$traslationAvailable = ['de', 'es', 'fr', 'id', 'it', 'ja', 'ko', 'pt', 'ru', 'vi', 'zh', 'th'];

foreach($traslationAvailable as $row => $key) {
 
  $string = file_get_contents("../notTranslated/$key.json");
  $string = json_decode($string, TRUE);
  
  $compared = file_get_contents("../onSpreadSheet/maintext.json");
  $compared = json_decode($compared, TRUE);
  // print_r($compared);
  // die();
  $object  = new findTho($string, $compared);
  $object->mapingData();
  $new = json_encode($object->newValue, JSON_UNESCAPED_UNICODE);
  $myfile = fopen("./newNotTranslated/$key.json", "w") or die("Unable to open file!");
  fwrite($myfile, $new);
  fclose($myfile);
}

// $string = file_get_contents("../notTranslated/de.json");
//   $string = json_decode($string, TRUE);
  
//   $compared = file_get_contents("../onSpreadSheet/maintext.json");
//   $compared = json_decode($compared, TRUE);
//   // print_r($compared);
//   // die();
//   $object  = new findTho($string, $compared);
//   $object->mapingData();
//   $new = json_encode($object->newValue, JSON_UNESCAPED_UNICODE);
  
//   print_r($new);